//
//  ViewController-table.swift
//  map4
//
//  Created by dasha on 7/7/17.
//  Copyright © 2017 dasha. All rights reserved.
//

import UIKit
import MapKit

class ViewController_table: UIViewController, UITableViewDelegate, UITableViewDataSource {
        
    @IBOutlet var tableView: UITableView!

        let cellReuseIdentifier = "cell"
    
        override func viewDidLoad() {
            super.viewDidLoad()
            self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
            tableView.delegate = self
            tableView.dataSource = self
        }
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return list.count
        }
    
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell!
            let pointsList = list[indexPath.row]

            cell.textLabel?.text = pointsList
            return cell
        }
    
}
