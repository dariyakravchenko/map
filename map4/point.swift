//
//  point.swift
//  map4
//
//  Created by dasha on 7/8/17.
//  Copyright © 2017 dasha. All rights reserved.
//

import Foundation
import MapKit

class Point: NSObject, MKAnnotation {
    let title: String?
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.coordinate = coordinate
        super.init()
    }
    
    var subtitle: String? {
        return title
    }
}


let point1 = Point(title: "Airport", coordinate: CLLocationCoordinate2DMake(50.412227, 30.443148))
let point2 = Point(title: "Post", coordinate: CLLocationCoordinate2DMake(50.413731, 30.443426))
let point3 = Point(title: "Stadium", coordinate: CLLocationCoordinate2DMake(50.415208, 30.443818))
let pointMyLocation = Point(title: "MyLocation", coordinate: CLLocationCoordinate2D())

var points = [point1, point2, point3, pointMyLocation]
var list: [String] = convertToString(array: points)


func convertToString (array: [Point]) -> [String] {
    var stringPoints: [String] = []
    for element in array {
        stringPoints.append("\(element.title!): \(Float(element.coordinate.latitude)), \(Float(element.coordinate.longitude))")
    }
    return stringPoints

}

