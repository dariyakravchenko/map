//
//  FirstViewController.swift
//  map4
//
//  Created by dasha on 7/6/17.
//  Copyright © 2017 dasha. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class FirstViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var myMap: MKMapView!
    
    let manager = CLLocationManager()
    var myPoint = CLLocationCoordinate2D()
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location =  locations[0]
        let span:MKCoordinateSpan = MKCoordinateSpanMake(0.02, 0.02)
        let myLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        myMap.setRegion(region, animated: true)
        
        myPoint = manager.location!.coordinate

        self.myMap.showsUserLocation = true

        points[3] = (Point(title: "myLocation", coordinate: CLLocationCoordinate2DMake(myPoint.latitude, myPoint.longitude)))
        
        manager.stopUpdatingLocation();
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        for item in points {
            myMap.addAnnotation(item)
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}







